console.log("Hello World");
 function addNumbers(a,b){
 	console.log("Displayed sum of 5 and 15:");
 	let sum = a+b;
 	console.log(sum);
 }
 addNumbers(5,15);

  function subtractNumbers(a,b){

 	console.log("Displayed difference of 20 and 5:");
 	let diff = a-b;
 	console.log(diff);
 }
 subtractNumbers(20,5);

 function productNumbers(a,b){

 	
 	let product = a*b;
 	return product;
 }


 function quotientNumbers(a,b){

 
 	let quotient = a/b;
 	return quotient;
 }


 let product = productNumbers(50,10);

 let quotient =  quotientNumbers(50,10);

console.log("The product of 50 and 10:");
 console.log(product);
 console.log("The quotient of 50 and 10");
 console.log(quotient);

 function areaCircle(r){
 	let area = 3.14159*(r**2);
 	return area;
 }

 let circleArea = areaCircle(15);
 console.log("The results of getting the area of a circle with radius 15 is:")
 console.log(circleArea);

 function average(p,q,r,s){
 	let average = (p+q+r+s)/4;
 	return average;
 }
 let averageVar = average(20,40,60,80);
 console.log("The average of 20,40,60,80:");
 console.log(averageVar);

  function pass(x,y){

  	let percentage = (x/y)*100;
  	let isPassed = (percentage>=75);
  	return isPassed;
  }


let isPassingScore = pass(38,50)
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);
